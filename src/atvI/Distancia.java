package atvI;

import java.util.Objects;

public class Distancia implements Comparable<Distancia> {
	private String origem;
	private String destino;
	private double dist;

	public Distancia(String origem, String destino, double dist) {
		this.origem = origem;
		this.destino = destino;
		this.dist = dist;
	}

	public String getOrigem() {
		return origem;
	}

	public String getDestino() {
		return destino;
	}

	public double getDist() {
		return dist;
	}

	@Override
	public String toString() {
		String format = "%-30s %-40s %s \n";
		String formatDados = "%-30s %-25s %s \n";
		return String.format(format, "Origem", "Destino", "KM") +
				String.format(formatDados, origem, destino, dist + " km");
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Distancia distancia = (Distancia) obj;
		return Double.compare(distancia.dist, dist) == 0 && origem.equals(distancia.origem)
				&& destino.equals(distancia.destino);
	}

	@Override
	public int hashCode() {
		return Objects.hash(origem, destino, dist);
	}

	@Override
	public int compareTo(Distancia outraDistancia) {
		return Double.compare(dist, outraDistancia.dist);
	}
}