**Projeto de Cálculo de Distâncias entre Cidades**

Este projeto tem como objetivo calcular e apresentar as distâncias entre várias cidades com base em um conjunto de dados fornecido em um arquivo CSV. Para isso, utilizaremos Streams e aplicaremos boas práticas de programação, como código limpo e refatoração, para garantir a legibilidade e manutenibilidade do código.

### Estrutura do Projeto

O projeto é dividido em quatro classes principais: `Distancia`, `Utils` ,`ViewDados` e a classe `Main`.

1. **Distancia.java:**

   A classe `Distancia` representa uma distância entre duas cidades. Ela possui três campos:
   - `origem` (String): A cidade de origem.
   - `destino` (String): A cidade de destino.
   - `dist` (double): A distância em quilômetros entre as duas cidades.

   Métodos implementados:
   - `toString()`: Retorna uma representação em string do objeto.
   - `equals()`: Verifica se dois objetos `Distancia` são iguais.
   - `hashcode()`: Gera um código hash único para o objeto.

2. **Utils.java:**

   A classe `Utils` contém um método estático que lê o conjunto de dados de um arquivo CSV e retorna uma lista de objetos `Distancia`.

   Métodos implementados:
   - `lerDistancias(String nomeArquivo)`: Lê o arquivo CSV especificado e retorna uma lista de objetos `Distancia`.

3. **ViewDados.java:**

 A classe `ViewDados` desempenha o papel de representar a interface visual deste projeto, utilizando a biblioteca Java Swing para criação da interface gráfica. Ela exibe um menu com várias opções.

   Opções do menu:
   - Listar todas as distâncias (simples).
   - Cinco distâncias impares.
   - Ordenar crescente.
   - Ordenar descrescente (mostrar apenas distância).

3. **Main.java:**

   A classe `Main` contém o ponto de entrada do programa. 

# Execução

<div style="display: flex; flex-wrap: wrap; gap: 10px;">
    <img src="../img/img1.png" width="40%">
    <img src="../img/img2.png" width="40%">
    <img src="../img/img3.png" width="40%">
    <img src="../img/img4.png" width="40%">
</div><br>

## Gif de execução
![Vídeo de demonstração](../img/video.gif)

# Diagrama de classe
 <img src="../img/diagrama1.png" width="70%">
