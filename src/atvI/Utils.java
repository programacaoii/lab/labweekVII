package atvI;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static List<Distancia> lerDistancias(String nomeArquivo) throws IOException {
        List<Distancia> distancias = new ArrayList<>();
        try (BufferedReader leitor = new BufferedReader(new FileReader(nomeArquivo))) {
            String linha;
            while ((linha = leitor.readLine()) != null) {
                String[] partes = linha.split(",");
                String origem = partes[0];
                String destino = partes[1];
                double dist = Double.parseDouble(partes[2]);
                distancias.add(new Distancia(origem, destino, dist));
            }
        }
        return distancias;
    }
}