package atvII;

public class Bairro {
	private String nome;
	private String data;
	private int obitos;
	private int casosConfirmados;

	public Bairro(String nome, int casosConfirmados, int obitos, String data) {
		this.nome=nome;
		this.casosConfirmados = casosConfirmados;
		this.obitos = obitos;
		this.data=data;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCasosConfirmados() {
		return casosConfirmados;
	}

	public void setCasosObitos(int obitos) {
		this.obitos = obitos;
	}

	public int getCasosObitos() {
		return obitos;
	}

	public void setCasosConfirmados(int casosConfirmados) {
		this.casosConfirmados = casosConfirmados;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public double calcularTaxaCrescimento() {
        if (casosConfirmados == 0) {
            return 0.0;
        }
        return ((double) (casosConfirmados - obitos)) / casosConfirmados * 100;
    }

    public double calcularTaxaLetalidade() {
        if (casosConfirmados == 0) {
            return 0.0;
        }
        return ((double) obitos) / casosConfirmados * 100;
    }
	@Override
	public String toString() {
	    return "Bairro: " + nome +
	           "\nCasos Confirmados: " + casosConfirmados +
	           "\nobitos: " + obitos +
	           "\nData: " + data +
	           "\nTaxa de Crescimento: " + String.format("%.2f", calcularTaxaCrescimento()) + "%" +
	           "\nTaxa de Letalidade: " + String.format("%.2f", calcularTaxaLetalidade()) + "%\n";
	}

}
