package atvII;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Covid {


	public static void listarBairrosMaisImpactadosPorCrescimento(List<Bairro> bairros) {
	    List<Bairro> bairrosOrdenadosPorCrescimento = new ArrayList<>(bairros);
	    bairrosOrdenadosPorCrescimento.sort(Comparator.comparingDouble(Bairro::calcularTaxaCrescimento).reversed());

	    System.out.println("Bairros mais impactados por taxa de crescimento:");
	    int contador = 1;
	    for (Bairro bairro : bairrosOrdenadosPorCrescimento) {
	        System.out.println(contador + ". " + bairro.getNome() + " - Taxa de Crescimento: " + 
	                           String.format("%.2f", bairro.calcularTaxaCrescimento()) + "%");
	        contador++;
	    }
	}

	public static void listarBairrosMaisImpactadosPorLetalidade(List<Bairro> bairros) {
	    List<Bairro> bairrosOrdenadosPorLetalidade = new ArrayList<>(bairros);
	    bairrosOrdenadosPorLetalidade.sort(Comparator.comparingDouble(Bairro::calcularTaxaLetalidade).reversed());

	    System.out.println("Bairros mais impactados por taxa de letalidade:");
	    int contador = 1;
	    for (Bairro bairro : bairrosOrdenadosPorLetalidade) {
	        System.out.println(contador + ". " + bairro.getNome() + " - Taxa de Letalidade: " + 
	                           String.format("%.2f", bairro.calcularTaxaLetalidade()) + "%");
	        contador++;
	    }
	}

    public static void exibirEstatisticasPorBairro(List<Bairro> bairros) {
        System.out.println("Estatisticas por bairro:");
        for (Bairro bairro : bairros) {
            System.out.println(bairro);
        }
    }
}
