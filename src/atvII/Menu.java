package atvII;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {

	public static void menu(String caminhoDoArquivo) {

		Scanner scanner = new Scanner(System.in);
		String caminhoArquivo = "src/arquivo.txt";

		List<Bairro> bairros = null;
		bairros = Utils.lerDadosDoArquivo(caminhoArquivo);

		int opcao;
		do {
			exibeMenu();
			opcao = scanner.nextInt();

			switch (opcao) {
			case 1:
				Covid.exibirEstatisticasPorBairro(bairros);
				break;
			case 2:
				Covid.listarBairrosMaisImpactadosPorCrescimento(bairros);
				System.out.println();
				Covid.listarBairrosMaisImpactadosPorLetalidade(bairros);
				break;
			case 3:
				incluirNovoRegistro(scanner, caminhoArquivo, bairros);
				break;
			case 4:
				System.out.println("Saindo do programa.");
				break;
			default:
				System.out.println("Opcao invalida.");
			}

		} while (opcao != 4);

		scanner.close();
	}

	private static void exibeMenu() {
		System.out.println("Menu:");
		System.out.println("1. Exibir estatisticas por bairro");
		System.out.println("2. Listar bairros mais impactados");
		System.out.println("3. Incluir novo registro");
		System.out.println("4. Sair");
		System.out.print("Escolha uma opcao: ");
	}

	private static void incluirNovoRegistro(Scanner scanner, String caminhoArquivo, List<Bairro> bairros) {
		System.out.print("Digite o nome do bairro: ");
		String nome = scanner.next();

		System.out.print("Digite o numero de casos confirmados: ");
		int casosConfirmados = scanner.nextInt();

		System.out.print("Digite o número de obitos: ");
		int obitos = scanner.nextInt();

		System.out.print("Digite a data da analise: ");
		String data = scanner.next();

		Bairro novoBairro = new Bairro(nome, casosConfirmados, obitos, data);
		bairros.add(novoBairro);

		Utils.escreverDadosNoArquivo(caminhoArquivo, novoBairro);
		System.out.println("Registro adicionado com sucesso.");
	}

}