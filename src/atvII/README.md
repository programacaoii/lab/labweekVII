# Analisador de Evolução da COVID-19 nos Bairros do Rio de Janeiro

Este é um programa Java desenvolvido para auxiliar os gestores da Secretaria de Saúde do Rio de Janeiro na análise da evolução da contaminação pela COVID-19 em vários bairros da cidade.

## Funcionalidades

- **Leitura de Dados:** O programa lê um arquivo contendo os dados de evolução da contaminação pela COVID-19 nos bairros do Rio de Janeiro.

- **Cálculo de Estatísticas:** Calcula a taxa de crescimento de casos confirmados e a taxa de letalidade para cada bairro.

- **Identificação dos Bairros Mais Impactados:** Gera uma lista dos bairros com maior taxa de crescimento e maior letalidade.

- **Exibição de Resultados:** Apresenta as estatísticas e a lista de bairros mais impactados de forma organizada e amigável.

- **Inclusão de Novos Registros:** Permite a adição de novos registros no arquivo de dados.


# Execução

<div style="display: flex; flex-wrap: wrap; gap: 10px;">
    <img src="../img/img2.1.png" width="40%">
    <img src="../img/img2.2.png" width="60%">
    <img src="../img/img2.3.png" width="60%">
</div><br>


# Diagrama de classe
 <img src="../img/diagrama2.png" width="70%">