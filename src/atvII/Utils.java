package atvII;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Utils {
	static List<Bairro> bairros = new ArrayList<>();

	public static List<Bairro> lerDadosDoArquivo(String caminhoArquivo) {
	    List<Bairro> bairros = new ArrayList<>();

	    try (BufferedReader leitor = new BufferedReader(new FileReader(caminhoArquivo))) {
	        String linha;
	        while ((linha = leitor.readLine()) != null) {
	            String[] partes = linha.split(",");
	            if (partes.length == 4) {
	                String nome = partes[0];
	                int casosConfirmados = Integer.parseInt(partes[1]);
	                int obitos = Integer.parseInt(partes[2]);
	                String data = partes[3];
	                Bairro bairro = new Bairro(nome, casosConfirmados, obitos, data);
	                bairros.add(bairro);
	            }
	        }
	    } catch (IOException e) {
	        System.err.println("Erro ao ler o arquivo: " + e);
	    }

	    return bairros;
	}

	

	List<Bairro> getBairros() {
		return bairros;
	}

	public static void escreverDadosNoArquivo(String caminhoArquivo, Bairro bairro) {
	    try (BufferedWriter escritor = new BufferedWriter(new FileWriter(caminhoArquivo, true))) {
	        String linha ="\n"+bairro.getNome() + "," + bairro.getCasosConfirmados() + ","
	                + bairro.getCasosObitos() + "," + bairro.getData();
	        escritor.write(linha);  
	    } catch (IOException e) {
	        System.out.println("Erro ao escrever o arquivo: " + e);
	    }
	}


}